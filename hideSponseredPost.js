// ==UserScript==
// @name         Hide Twitter Sponsored Posts
// @namespace    http://tampermonkey.net/
// @version      0.1
// @description  Hide sponsored posts on Twitter.
// @author       Scan
// @match        https://x.com/*
// @icon         https://www.google.com/s2/favicons?sz=64&domain=twitter.com
// @grant        none
// ==/UserScript==

(function () {
	'use strict';

	let observer = new MutationObserver(hideAds);
	observer.observe(document, { childList: true, subtree: true });

	// List of possible translations for "Sponsored"
	const sponsoredTexts = [
		'Patrocinat',
		'Patrocinado',
		'Sponsored',
		'Gesponsert',
		'Sponsorisé',
		'Patrocinato',
		'スポンサー付',
		'赞助内容'
	];

	function hideAds() {
		let articles = document.querySelectorAll('article');

		articles.forEach(article => {
			let spans = article.querySelectorAll('span');
			for (let span of spans) {
				// Check if the text of the span is in the list of sponsored texts
				if (
					sponsoredTexts.some(sponsoredText =>
						span.textContent.includes(sponsoredText)
					)
				) {
					article.style.display = 'none';
					break;
				}
			}
		});
	}

	hideAds(); // Hide any ads that may have loaded before this script.
})();
