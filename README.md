# Hide Twitter Sponsored Posts

This is a Tampermonkey script designed to hide sponsored ads on Twitter. It works on both the desktop and mobile web versions, offering a cleaner and uninterrupted user experience free from commercial breaks.

## Getting Started

To use this script, you will need Tampermonkey installed, which is an extension available for most web browsers, including Chrome, Firefox, Safari, and more.

### Installing Tampermonkey

1. Install Tampermonkey from your preferred browser's extension store.
2. Once installed, you should see the Tampermonkey icon at the top right corner of your browser.

### Adding the Script

1. Open Tampermonkey in your browser and navigate to the "Dashboard".
2. Click on "New Script...".
3. Copy and paste the code for the "Hide Twitter Sponsored Posts" script into the editor.
4. Save the script by clicking "File" > "Save" or by pressing Ctrl+S.

## How It Works

The script searches for page elements on Twitter's web pages that correspond to sponsored ads and hides them from the user's view. This is done without affecting the functionality of the page or the display of other, non-sponsored content.

## Contributing

If you would like to contribute to the project, please feel free to fork the repository and submit your pull requests. You can also open an issue if you find any bugs or have a suggestion for improvement.

## License

This script is distributed under the MIT License. See the `LICENSE` file for more details.